<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lop extends Model
{
    protected $table = 'lop';
    protected $fillable = [
        'ten_lop',
        'ma_khoa',
        'ma_nganh'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';

    public function khoa()
    {
        return $this -> belongsTo('App\Models\LienKhoa','ma_khoa');
    }
    public function nganh_hoc()
    {
        return $this -> belongsTo('App\Models\NganhHoc','ma_nganh');
    }
}
