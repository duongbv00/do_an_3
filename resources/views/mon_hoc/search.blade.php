@extends('layout.master.master')
@section('search')
    <div class="search-bar">
        <form action="{{ route('mon_hoc.search') }}" method="POST" class="search-bar ">
            @csrf
            <input class="search-input form-control" type="search" name="search" placeholder="Tìm kiếm môn học" />
            <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                <i class="mdi mdi-close-circle"></i>
            </a>
        </form>
    </div>
@endsection
@section('content')

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
    </head>
    <body>
    <div>
        @if (Session::has('success'))
            <script>
                alert("Thêm thành công.");
            </script>
        @elseif (Session::has('success_update'))
            <script>
                alert("Sửa thành công.");
            </script>
        @endif
    </div>
    <h3>Danh Sách Môn Học</h3>
    <br>
    <br>
    <table class="table table-bordered mb-0" style="text-align: center; width: 70%;">
        <tr>
            <th>Mã</th>
            <th>Tên môn học</th>
            <th>Tác vụ</th>
        </tr>
        @foreach ($array_mon_hoc as $mon_hoc)
            <tr>
                <td>
                    {{ $mon_hoc -> ma }}
                </td>
                <td>
                    {{ $mon_hoc -> ten_mon }}
                </td>
                <td>
                    <table style="width: 100%">
                        <td>
                            <a href="{{ route('mon_hoc.view_update',['ma' => $mon_hoc->ma]) }}">
                                <i class="mdi mdi-pencil-outline"></i>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('mon_hoc.delete',['ma' => $mon_hoc -> ma]) }}">
                                <i class="dripicons-trash"></i>
                            </a>
                        </td>
                    </table>
                </td>
            </tr>
        @endforeach
    </table>
    <br>
    <span style="text-align: center">{{ $array_mon_hoc -> render() }}</span>
    </body>
    </html>
@endsection
