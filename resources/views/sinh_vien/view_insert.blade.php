@extends('layout.master.master')
@section('content')
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript">
        function validateForm() {
            var name = document.getElementById('name').value;
            var sdt = document.getElementById('sdt').value;
            var email = document.getElementById('email').value;
            var lop = document.getElementById('lop').value;
            if (name == "") {
                alert("Tên không được để trống.");
                return false;
            }
            if (sdt == "") {
                alert("số điện thoại không được để trống.");
                return false;
            }
            if (email == "") {
                alert("email không được để trống.");
                return false;
            }
            if (lop == "0") {
                alert("lớp chưa được chọn.");
                return false;
            }
            return true;
        }
    </script>
</head>
<body>
    <form method="POST" action="{{ route('sinh_vien.process_insert') }}" class="custom-validation" onsubmit="return validateForm()">
        {{ csrf_field() }}
        <h3>Thêm sinh viên</h3>
        <br>
        <br>
        <div class="form-group">
            <label>Họ Tên</label>
            <input class="form-control" type="text" name="ho_ten" id="name" style="width: 40%;">
        </div>

        <div class="form-group">
            <label>Giới Tính</label> &nbsp;&nbsp;&nbsp;
            <input class="" type="radio" name="gioi_tinh" value ="0">&nbsp;Nam&nbsp;&nbsp;
            <input class="" type="radio" name="gioi_tinh" value ="1">&nbsp;Nữ
        </div>

        <div class="form-group">
            <label>Ngày Sinh</label>
            <input class="form-control" type="date" name="ngay_sinh" style="width: 40%;">
        </div>

        <div class="form-group">
            <label>Địa Chỉ</label>
            <input class="form-control" type="text" name="dia_chi" style="width: 40%;">
        </div>

        <div class="form-group">
            <label>Số Điện Thoại</label>
            <input class="form-control" id="sdt" type="text" name="so_dien_thoai" style="width: 40%;">
        </div>

        <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="email" id="email" name="email" style="width: 40%;">
        </div>
        <div class="form-group">
            <label>lớp</label>
            <select name = "ma_lop" class="form-control" id="lop" style="width: 40%; text-align: center;">
                <option selected value="0">--Chọn--</option>
                @foreach ($lop as $lop)
                    <option value="{{ $lop -> ma }}">
                        {{ $lop -> ten_lop }}
                    </option>
                @endforeach
            </select>
        </div>
        <button class="btn btn-primary waves-effect waves-light mr-1">Thêm</button>
        <button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
    </form>
</body>
</html>
@endsection