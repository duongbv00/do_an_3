<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LienKhoa;

class KhoaController extends Controller
{
    public function view_all()
    {
        $array_khoa = LienKhoa::paginate(10);
        return view('khoa.view_all',compact('array_khoa'));
    }
    public function view_insert()
    {
        return view('khoa.view_insert');
    }
    public function process_insert(Request $rq)
    {
        $khoa = $rq -> ten_khoa;
        if(LienKhoa::where('ten_khoa','=',$khoa) -> first()){
            return redirect() -> route('khoa.view_insert') -> with('erro','tên lày Đã Tồn Tại');
        }
        LienKhoa::create($rq -> all());
        return redirect('khoa') -> with('session','/');
    }
    public function view_update($ma,Request $rq)
    {
        $khoa = LienKhoa::find($ma);
        return view('khoa.view_update',compact('khoa'));
    }
    public function update($ma,Request $rq)
    {
        $khoa = $rq -> ten_khoa;
        if(LienKhoa::where('ma','=',$ma) -> where('ten_khoa','=',$khoa) ->first()){
            LienKhoa::find($ma) -> update($rq -> all());
        }
        else if(LienKhoa::where('ten_khoa','=',$khoa) -> first()){
            return redirect() -> route('khoa.view_update',['ma' => $ma]) -> with('erro','tên lày Đã Tồn Tại');
        }
        LienKhoa::find($ma) -> update($rq -> all());
        return redirect('khoa') -> with('session_update','/');
    }
    public function delete($ma)
    {
        LienKhoa::destroy($ma);
        return redirect('khoa');
    }
    public function search(Request $rq){
        $data = $rq -> search;
        $array_khoa = LienKhoa::where('ten_khoa','LIKE', '%'.$data.'%') -> paginate(10);

        return view('khoa.search',compact('array_khoa'));
    }
}
