<?php

namespace App\Imports;

use App\Models\SinhVien;
use App\Models\Lop;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SinhVienImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $row = array_filter($row);
        if(!empty($row)){
            $array = [
                'ho_ten' => $row['ho_ten'],
                'gioi_tinh' => ($row['gioi_tinh']=='Nam') ? 0 : 1,
                'ngay_sinh' => \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['ngay_sinh'])->format('Y-m-d'),
                'dia_chi' => $row['dia_chi'],
                'so_dien_thoai' => $row['so_dien_thoai'],
                'email' => $row['email'],
                'ma_lop' => Lop::firstOrCreate(['ten_lop' => $row['lop']]) -> ma,
            ];
            return new SinhVien($array);
        }
    }
}
