<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('lop', function(Blueprint $TABLE){
            $TABLE -> INCREMENTS('ma');
            $TABLE -> STRING('ten_lop',50);
            $TABLE -> INTEGER('ma_khoa') -> unsigned();
            $TABLE -> INTEGER('ma_nganh') -> unsigned();

            $TABLE -> foreign('ma_khoa') -> references('ma') -> on('lien_khoa')-> onDelete('cascade');
            $TABLE -> foreign('ma_nganh') -> references('ma') -> on('nganh_hoc')-> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
