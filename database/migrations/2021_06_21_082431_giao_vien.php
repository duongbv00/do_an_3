<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GiaoVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giao_vien',function(Blueprint $TABLE){
            $TABLE -> INCREMENTS('ma');
            $TABLE -> STRING('ho_ten',50) -> nullable();
            $TABLE -> BOOLEAN('gioi_tinh') -> nullable();
            $TABLE -> DATE('ngay_sinh') -> nullable();
            $TABLE -> TEXT('dia_chi')-> nullable();
            $TABLE -> TEXT('email') -> unique();
            $TABLE -> TEXT('password');
            $TABLE -> STRING('token') -> nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
