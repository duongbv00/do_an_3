<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\LienKhoa;
use App\Models\NganhHoc;

class LopController extends Controller
{
    public function view_all()
    {
        $array_lop = Lop::with('nganh_hoc','khoa') -> paginate(10);
        return view('lop.view_all',compact('array_lop'));
    }
    public function view_insert()
    {
        $array_nganh = NganhHoc::get();
        $array_khoa = LienKhoa::orderByDesc('ma','Desc') -> get();
        return view('lop.view_insert',compact('array_nganh','array_khoa'));
    }
    public function process_insert(Request $rq)
    {
        $lop = $rq -> ten_lop;
        if(lop::where('ten_lop','=',$lop) -> first()){
            return redirect() -> route('lop.view_insert') -> with('error','/');
        }
        Lop::create($rq -> all());
        return redirect('lop') -> with('success','/');
    }
    public function view_update($ma,Request $rq)
    {
        $lop = Lop::find($ma);
        $array_nganh = NganhHoc::get();
        $array_khoa = LienKhoa::get();
        return view('lop.view_update',compact('lop','array_nganh','array_khoa'));
    }
    public function update($ma,Request $rq)
    {
        $lop = $rq -> ten_lop;
        if(lop::where('ten_lop','=',$lop) -> where('ma','=',$ma) -> first()){
            Lop::find($ma) -> update($rq -> all());
        }
        else if(lop::where('ten_lop','=',$lop) -> first()){
            return redirect() -> route('lop.view_update',['ma' => $ma]) -> with('error','/');
        }
        Lop::find($ma) -> update($rq -> all());
        return redirect('lop');
    }
    public function delete($ma)
    {
        lop::destroy($ma);
        return redirect('lop');
    }
    public function search(Request $rq){
        $data = $rq -> search;
        $array_lop = Lop::where('ten_lop','LIKE', '%'.$data.'%')
                    -> join('lien_khoa','lop.ma_khoa','=','lien_khoa.ma')
                    -> join('nganh_hoc','lop.ma_nganh','=','nganh_hoc.ma')
                    -> orwhere('lien_khoa.ten_khoa','LIKE', '%'.$data.'%')
                    -> orwhere('nganh_hoc.ten_nganh','LIKE', '%'.$data.'%')
                    -> paginate(10);

        return view('lop.search',compact('array_lop'));
    }
}
