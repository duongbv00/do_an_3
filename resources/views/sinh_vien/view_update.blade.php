@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('name').value;
			var sdt = document.getElementById('sdt').value;
			var email = document.getElementById('email').value;
			var lop = document.getElementById('lop').value;
			if (name == "") {
				alert("Tên không được để trống.");
				return false;
			}
			if (sdt == "") {
				alert("số điện thoại không được để trống.");
				return false;
			}
			if (email == "") {
				alert("email không được để trống.");
				return false;
			}
			if (lop == "0") {
				alert("lớp chưa được chọn.");
				return false;
			}else{
				var result = confirm("Bạn có muốn chỉnh sửa không ?");
				if(result == true){
					return true;
				}else {
					return false;
				}
			}
		}
	</script>
</head>
<body>
	<h3>sửa thông tin</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('sinh_vien.update',['ma' => $sinh_vien -> ma]) }}" onsubmit="return validateForm()" class="table">
		{{ csrf_field() }}
		<div class="form-group">
			<label>Họ Tên</label>
			<input class="form-control" type="text" name="ho_ten" value="{{ $sinh_vien -> ho_ten }}" id="name" style="width: 40%;">
		</div>

		<div class="form-group">
			<label>Giới Tính</label> &nbsp;&nbsp;&nbsp;
			<input type="radio" name="gioi_tinh" value ="0"
			@if ($sinh_vien -> gioi_tinh == 0)
				checked
			@endif>Nam &ensp;
			<input type="radio" name="gioi_tinh" value ="1"
				   @if ($sinh_vien -> gioi_tinh ==1)
				   checked
					@endif
			>Nữ
		</div>

		<div class="form-group">
			<label>Ngày Sinh</label>
			<input class="form-control" type="date" name="ngay_sinh" value="{{ $sinh_vien -> ngay_sinh }}" style="width: 40%;">
		</div>

		<div class="form-group">
			<label>Địa Chỉ</label>
			<input class="form-control" type="text" name="dia_chi" value="{{ $sinh_vien -> dia_chi }}" style="width: 40%;">
		</div>

		<div class="form-group">
			<label>Số Điện Thoại</label>
			<input class="form-control" id="sdt" type="text" name="so_dien_thoai" value="{{ $sinh_vien -> so_dien_thoai }}" style="width: 40%;">
		</div>

		<div class="form-group">
			<label>Email</label>
			<input class="form-control" type="email" id="email" name="email" value="{{ $sinh_vien -> email }}" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>lớp</label>
			<select name = "ma_lop" class="form-control" id="lop" style="width: 40%; text-align: center;">
				@foreach ($lop as $array_lop)
					<option value="{{ $array_lop -> ma}}"
							@if ($sinh_vien -> ma_lop  == $array_lop -> ma )
							selected
							@endif>{{ $array_lop -> ten_lop }}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Sửa</button>
		<button type="reset" class="btn btn-secondary waves-effect">Khôi phục</button>
	</form>
</body>
</html>
@endsection