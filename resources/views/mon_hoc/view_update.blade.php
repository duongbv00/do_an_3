@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var mon_hoc = document.getElementById('mon_hoc').value;
			if (mon_hoc == "") {
				alert('Tên không được bỏ trống');
				return false;
			}else{
				var result = confirm("Bạn có muốn chỉnh sửa không ?");
				if(result == true){
					return true;
				}else {
					return false;
				}
			}
		}
	</script>
</head>
<body>
	<h3>Sửa Thông Tin</h3>
	<br>
	<br>
	<form method="POST"  action="{{ route('mon_hoc.update',['ma' => $mon_hoc -> ma]) }}" class="custom-validation" onsubmit="return validateForm()">
		{{ csrf_field() }}
		<div class="form-group">
			<label>Nhập tên môn học</label>
			<input type="text" name="mon_hoc" id="mon_hoc" class="form-control" style="width: 40%;" value="{{ $mon_hoc -> ten_mon }}">
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Sửa</button>
		<button type="reset" class="btn btn-secondary waves-effect">Khôi phục</button>
	</form>
</body>
</html>
@endsection