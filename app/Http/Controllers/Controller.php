<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller as BaseController;
use App\Models\GiaoVu;
use App\Models\GiaoVien;
use Exception;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function giao_vu()
    {
        return view('giao_vu');
    }
    public function view_login()
    {
        return view('view_login');
    }
    public function giao_vien()
    {
        return view('giao_vien');
    }

    public function process_login(Request $rq)
    {
        try {
            $giao_vu = GiaoVu::where('email',$rq -> email)
                -> where('password',$rq -> password) -> firstOrFail();

            Session::put('ma_giao_vu',$giao_vu -> ma);
            Session::put('ho_ten',$giao_vu -> ho_ten);

            return redirect() -> route('giao_vu');

        } catch (Exception $e) {

            try {
                $giao_vien = GiaoVien::where('email',$rq -> email)
                    -> where('password',$rq -> password) -> first();

                Session::put('ma_giao_vien',$giao_vien -> ma);
                Session::put('ho_ten',$giao_vien -> ho_ten);
                return redirect() -> route('giao_vien');

            } catch (Exception $e) {
                return redirect() -> route('view_login') -> with('erro','/');
            }
        }

    }
    public function forgot_password(){
        return view('forgot_password');
    }
    public function sendemail(Request $_rq){
        $data = $_rq -> email;
        $now =Carbon::now('Asia/Ho_Chi_Minh') -> format('d-m-Y');
        $title_email = "Lấy lại mật khẩu".''.$now;
        $email = GiaoVien::where('email','=',$data) -> get();
        foreach ( $email as $key => $value){
            $ma = $value -> ma;
        }
        if($email){
            $count_email = $email -> count();
            if ($count_email == 0){
                return redirect() -> route('forgot_password') -> with('error','/');
            }else {
                $token_random = Str::random(20);
                $email = GiaoVien::find($ma);
                $email->token = $token_random;
                $email->save();

                $to_email = $data;
                $link_reset_pass = url('/update-new-pass?email=' . $to_email . '&token=' . $token_random);
                $data_email = array(
                    "name" => $title_email,
                    "body" => $link_reset_pass,
                    'email' => $data
                );
                Mail::send('forget_pass_notify', ['data' => $data_email], function ($message) use ($title_email, $data) {
                    $message->to($data)->subject($title_email);
                    $message->from($data, $title_email);
                });
            }
        }
    }
    public function logout()
    {
        Session::flush();
        return redirect() -> route('view_login');
    }
}
