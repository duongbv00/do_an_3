@extends('layout.master.master-gv')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<H1>Danh sách phân công</H1>
	<br>
	<table class="table table-bordered mb-0" style="text-align: center;">
		<tr>
			<th>Lớp</th>
			<th>Môn học</th>
		</tr>
		@foreach ($array_phan_cong as $phan_cong)
			<tr>
				<td>
					{{ $phan_cong -> lop -> ten_lop }}
				</td>
				<td>
					{{ $phan_cong -> mon_hoc -> ten_mon}}
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>
@endsection