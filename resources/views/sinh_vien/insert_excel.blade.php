@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div ng-app="app">
		<div ng-controller="UploadController as vm">
			<div class="container">
				<div class="page-header">
					<h1>Thêm sinh viên </h1>
				</div>
				<br>
				<div class="alert alert-info">
					<p>Tải file mẫu tại đây: </p>
					<a href="{{asset('file/file_mau.xlsx')}}" download="mau.xlsx">Dowload</a>
				</div>
				<form method="POST" enctype="multipart/form-data" action="{{ route('sinh_vien.process_excel') }}" onsubmit="return validateForm()">
					{{ csrf_field() }}
					<label>Chọn file excel:</label>
					<input type="file" name="file_excel" class="form-control" id="excel" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
					<br>
					<br>
					<button class="btn btn-primary">Thêm</button>
					<button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
				</form>
			</div>
		</div>
	</div>

</body>
</html>
@endsection

