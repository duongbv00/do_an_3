<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LienKhoa extends Model
{
    protected $table = 'lien_khoa';
    protected $fillable = [
        'ten_khoa',
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
