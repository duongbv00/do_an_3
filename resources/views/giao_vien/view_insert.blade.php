@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html lang="">
<head>
	<style>
		#show{
			position: relative;
			bottom: 30px;
			left: 500px;
		}
		span {
			color: #707070;
			cursor: pointer;
		}
	</style>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('ho_ten').value;
			var email = document.getElementById('email').value;
			var password = document.getElementById('password').value;
			if (name == "") {
				alert("Tên không được để trống:");
				return false;
			}
			if (email == "") {
				alert("Email không được để trống:");
				return false;
			}
			if (password == "") {
				alert("Password không được để trống.");
				return false;
			}
			return true;
		}

		isBoon = true;
		function ShowHidden(){
			if(isBoon){
				document.getElementById("password").setAttribute("type","text");
				isBoon = false;
			}else {
				document.getElementById("password").setAttribute("type","password");
				isBoon = true;
			}
		}
	</script>
</head>
<body>
	<h3>Đăng Kí Tài Khoản Cho Giáo Viên</h3>
	<br>
	<br>
	<form method="POST" onsubmit="return validateForm()" class="custom-validation" action="{{ route('GiaoVien.process_insert') }}">
		@csrf
		@if (Session::has('erro'))
			<script>
				alert("Email đã tồn tại !!!");
			</script>
		@endif
		<div class="form-group">
			<label>Họ tên</label>
			<input type="text" name="ho_ten" id="ho_ten" class="form-control" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="email" name="email" id="email" class="form-control" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>Password</label>
			<input type="password" name="password" id="password" class="form-control" style="width: 40%;">
			<span id = "show">
				<i class="far fa-eye" onclick="ShowHidden()"></i>
			</span>
		</div>
		<button type="submit" class="btn btn-primary waves-effect waves-light mr-1">Đăng kí</button>
		<button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
	</form>
</body>
</html>
@endsection