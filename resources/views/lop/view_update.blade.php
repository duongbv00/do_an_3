@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('lop').value;
			if (name == "") {
				alert('Tên không được bỏ trống');
				return false;
			}else{
				var result = confirm("Bạn có muốn chỉnh sửa không ?");
				if(result == true){
					return true;
				}else {
					return false;
				}
			}
			return true;
		}
	</script>
</head>
<body>
<div>
	@if (Session::has('error'))
		<script>
			alert("Lớp lày đã tồn tại.");
		</script>
	@endif
</div>
	<h3>Sửa thông tin</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('lop.update',['ma' => $lop -> ma]) }}" onsubmit="return validateForm()">
		{{ csrf_field() }}
		<div class="form-group">
			<label>Tên lớp học</label>
			<input type="text" name="ten_lop" id="lop"  class="form-control" value="{{ $lop -> ten_lop }}" style="width: 30%;">
		</div>
		<div class="form-group">
			<label>Liên khóa</label>
			<select name="ma_khoa" class="form-control" style="width: 15%">
				@foreach ($array_khoa as $khoa)
					<option value="{{ $khoa -> ma }}"
							@if ($lop -> ma_khoa == $khoa -> ma)
							selected
							@endif>
						{{ $khoa -> ten_khoa }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Ngành học</label>
			<select name="ma_nganh" class="form-control" style="width: 15%;">
				@foreach ($array_nganh as $nganh)
					<option value="{{ $nganh -> ma }}"
							@if ($lop -> ma_nganh == $nganh -> ma )
							selected
							@endif>
						{{ $nganh -> ten_nganh }}
					</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Sửa</button>
		<button type="reset" class="btn btn-secondary waves-effect">Khôi phục</button>
	</form>
</body>
</html>
@endsection