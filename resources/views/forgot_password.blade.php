
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Lấy lại mật khẩu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Bootstrap Css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />

</head>

<body>

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<!-- Begin page -->
<div class="accountbg" style="background: url('{{asset("images/bg.jpg")}}');background-size: cover;background-position: center;"></div>

<div class="account-pages mt-5 pt-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-5">
                <div class="card">
                    <div class="card-body">
                        <div class="p-3">
                            <h4 class="font-size-18 mt-2 text-center">Lấy lại mật khẩu</h4>
                            <p class="text-muted text-center mb-4">Enter your Email and instructions will be sent to you!</p>

                            <form class="form-horizontal" action="{{ route('sendemail') }}" method="post">
                                @csrf
                                @if (Session::has('error'))
                                    <script>
                                        alert("Email này chưa được đăng kí.");
                                    </script>
                                @endif
                                <div class="form-group">
                                    <label for="useremail">Email</label>
                                    <input type="email" class="form-control" id="useremail" name="email" placeholder="Nhập Email">
                                </div>

                                <div class="form-group">
                                    <div class="text-right">
                                        <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Reset</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
                <div class="mt-5 text-center">
                    <p class="text-white"><a href="{{route('view_login')}}" class="font-weight-bold text-primary"> Đăng nhập tại đây </a> </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- JAVASCRIPT -->
<script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('libs/node-waves/waves.min.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>
