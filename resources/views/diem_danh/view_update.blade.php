@extends('layout.master.master-gv')

@section('content')

		<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
@if (Session::has('error'))
	<h1>{{ Session::get('error') }}</h1>
@endif
<h3>Điểm Danh Sinh Viên</h3>
<br>
<br>
<form method="POST" action="#" class="custom-validation" style="margin-left: 40%">
	{{ csrf_field() }}
	<div class="form-group">
		<label style="margin-left: 100px;">Lớp</label>
		<select name="ma_lop" class="form-control" style="width: 30%;padding-left: 75px">
				<option value="{{ $array_lop -> ma }}">
					{{ $array_lop -> ten_lop }}</option>
		</select>
	</div>
	{{--<div class="form-group">
		<label style="margin-left: 80px">Môn học</label>
		<select name="ma_mon_hoc" class="form-control" style="width: 30%;padding-left: 75px">
			@foreach ($array_mon as $mon_hoc)
				<option value="{{ $mon_hoc -> ma }}" @if ($mon_hoc -> ma == $ma_mon_hoc)
				selected
						@endif> {{ $mon_hoc -> ten_mon }} </option>
			@endforeach
		</select>
	</div>--}}
</form>
<br>
<br>
</body>
</html>
@endsection