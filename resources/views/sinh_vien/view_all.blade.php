@extends('layout.master.master')
@section('search')
	<div class="search-bar">
		<form action="{{ route('sinh_vien.search') }}" method="POST" class="search-bar ">
			@csrf
			<input class="search-input form-control" type="search" name="search" placeholder="Tìm kiếm sinh viên" />
			<a href="#" class="close-search toggle-search" data-target="#search-wrap">
				<i class="mdi mdi-close-circle"></i>
			</a>
		</form>
	</div>
@endsection

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
</head>
<body>
<div>
	@if (Session::has('session'))
		<script>
			alert("Thêm thành công.");
		</script>
	@elseif (Session::has('session_update'))
		<script>
			alert("Sửa thành công.");
		</script>
	@elseif (Session::has('search'))
		<script>
			alert("Không tìm thấy dữ liệu.");
		</script>
	@endif

	<form method="post" action="{{ route('sinh_vien.delete') }}">
		@csrf
		<h3>Danh Sách Sinh Viên</h3>
		<br>
		<br>
		<br>
		<table class="table table-bordered mb-0" id="sinh_vien" style="text-align: center;">
			<tr>
				<th>Họ tên</th>
				<th>Giới tính</th>
				<th>Ngày sinh</th>
				<th>Đỉa chỉ</th>
				<th>Số điện thoại</th>
				<th>Email</th>
				<th>Lớp</th>
				<th>Tác vụ</th>
			</tr>
			@foreach ($array_sinh_vien as $sinh_vien)
				<tr>
					<td>
						{{ $sinh_vien -> ho_ten }}
					</td>
					<td>
						@if ($sinh_vien -> gioi_tinh == 0)
							Nam
						@else
							Nữ
						@endif
					</td>
					<td>
						{{ $sinh_vien -> ngay_sinh }}
					</td>
					<td>
						{{ $sinh_vien -> dia_chi }}
					</td>
					<td>
						{{ $sinh_vien -> so_dien_thoai }}
					</td>
					<td>
						{{ $sinh_vien -> email }}
					</td>
					<td>
						{{ $sinh_vien -> ten_lop }}
					</td>
					<td>
						<table width="100%">
							<td>
								<a href="{{ route('sinh_vien.view_update',['ma' => $sinh_vien -> ma]) }}">
									<i class="mdi mdi-pencil-outline"></i>
								</a>
							</td>
							<td>
								<input type="checkbox" name="delete[{{ $sinh_vien -> ma }}]" value="{{ $sinh_vien -> ma }}">
							</td>
						</table>
					</td>
				</tr>
			@endforeach
		</table>
		<div style=" padding-left: 1165px;">
			<button class="btn btn-primary waves-effect waves-light mr-1">Xóa</button>
		</div>
	</form>
</div>
<div id="all"></div>
<br>
<span style="text-align: center">{{ $array_sinh_vien -> render() }}</span>
</body>
</html>
@endsection