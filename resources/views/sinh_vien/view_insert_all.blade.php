@extends('layout.master.master')

@section('content')
        <!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<h3>Thêm sinh viên</h3>
<br>
<br>
<br>
<div class="form-group">
    <a href="{{ route('sinh_vien.view_insert') }}" class="btn btn-primary waves-effect waves-light mr-1">Thêm thủ công</a>
    <a href="{{ route('sinh_vien.insert_excel') }}" class="btn btn-primary waves-effect waves-light mr-1">Thêm nhanh</a>
</div>
</body>
</html>
@endsection
