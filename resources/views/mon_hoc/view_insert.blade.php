@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var mon_hoc = document.getElementById('mon_hoc').value;
			if (mon_hoc == "") {
				alert('Tên không được bỏ trống');
				return false;
			}
		}
	</script>
</head>
<body>
	<h3>Thêm Môn Học</h3>
	<br>
	<br>
	<form method="POST" onsubmit="return validateForm()" action="{{ route('mon_hoc.process_insert') }}" class="custom-validation">
		{{ csrf_field() }}
		@if (Session::has('error'))
			<script !src="">
				alert("Tên đã tồn tại.");
			</script>
		@endif
		<div class="form-group">
			<label>Nhập tên môn học</label>
			<input type="text" name="ten_mon" id="mon_hoc" class="form-control" style="width: 40%;">
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Thêm</button>
		<button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
	</form>
</body>
</html>
@endsection