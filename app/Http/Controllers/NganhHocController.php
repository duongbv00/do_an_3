<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NganhHoc;

class NganhHocController extends Controller
{
    public function view_all()
    {
        $array_nganh = NganhHoc::paginate(10);
        return view('nganh_hoc.view_all',compact('array_nganh'));
    }
    public function view_insert()
    {
        return view('nganh_hoc.view_insert');
    }
    public function process_insert(Request $rq)
    {
        $nganh = $rq -> ten_nganh;
        if(NganhHoc::where('ten_nganh','=',$nganh) -> first()){
            return redirect() -> route('nganh_hoc.view_insert') -> with('error','/');
        }
        NganhHoc::create($rq -> all());
        return redirect('nganh_hoc') -> with('success','/');
    }
    public function view_update($ma,Request $rq)
    {
        $nganh_hoc = NganhHoc::find($ma);
        return view('nganh_hoc.view_update',compact('nganh_hoc'));
    }
    public function update($ma,Request $rq)
    {
        $nganh = $rq -> ten_nganh;
        if(NganhHoc::where('ma','=',$ma) -> where('ten_nganh','=',$nganh) ->first()){
            NganhHoc::find($ma) -> update($rq -> all());
        }
        else if(NganhHoc::where('ten_nganh','=',$nganh) -> first()){
            return redirect() -> route('nganh_hoc.view_update',['ma' => $ma]) -> with('error','/');
        }
        NganhHoc::find($ma) -> update($rq -> all());
        return redirect('nganh_hoc') -> with('success_update','/');
    }
    public function delete($ma)
    {
        NganhHoc::destroy($ma);
        return redirect('nganh_hoc');
    }
    public function search(Request $rq){
        $data = $rq -> search;
        $array_nganh = NganhHoc::where('ten_nganh','LIKE', '%'.$data.'%') -> paginate(10);

        return view('nganh_hoc.search',compact('array_nganh'));
    }
}
