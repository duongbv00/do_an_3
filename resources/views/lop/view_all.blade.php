@extends('layout.master.master')
@section('search')
	<div class="search-bar">
		<form action="{{ route('lop.search') }}" method="POST" class="search-bar ">
			@csrf
			<input class="search-input form-control" type="search" name="search" placeholder="Tìm kiếm lớp học" />
			<a href="#" class="close-search toggle-search" data-target="#search-wrap">
				<i class="mdi mdi-close-circle"></i>
			</a>
		</form>
	</div>
@endsection
@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div>
		@if (Session::has('success'))
			<script>
				alert("Thêm thành công.");
			</script>
		@elseif (Session::has('success_update'))
			<script>
				alert("Sửa thành công.");
			</script>
		@endif
	</div>
	<h3>Danh Sách lớp</h3>
	<br>
	<br>
	<table class="table table-bordered mb-0" style="text-align: center">
		<tr>
			<th>Mã lớp</th>
			<th>Tên lớp</th>
			<th>Khối</th>
			<th>ngành</th>
			<th>Tác vụ</th>
		</tr>
		@foreach ($array_lop as $lop)
			<tr>
				<td>
					{{ $lop -> ma }}
				</td>
				<td>
					{{ $lop -> ten_lop }}
				</td>
				<td>
					{{ $lop -> khoa -> ten_khoa }}
				</td>
				<td>
					{{ $lop -> nganh_hoc -> ten_nganh }}
				</td>
				<td>
					<table style="width: 100%">
						<td>
							<a href="{{ route('lop.view_update',['ma' => $lop->ma]) }}">
								<i class="mdi mdi-pencil-outline"></i>
							</a>
						</td>
						<td>
							<a href="{{ route('lop.delete',['ma' => $lop -> ma]) }}">
								<i class="dripicons-trash"></i>
							</a>
						</td>
					</table>
				</td>
			</tr>
		@endforeach
	</table>
	<span style="text-align: center">{{ $array_lop -> render() }}</span>
</body>
</html>
@endsection