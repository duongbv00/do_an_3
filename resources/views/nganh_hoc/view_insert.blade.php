@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('nganh').value;
			if (name == "") {
				alert("Tên không được để trống:");
				return false;
			}
			return true;
		}
	</script>
</head>
<body>
@if (Session::has('error'))
	<script>
		alert("Tên lày đã tồn tại");
	</script>
@endif
	<h3>Thêm Ngành Học</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('nganh_hoc.process_insert') }}" onsubmit="return validateForm()">
		@csrf
		<div class="form-group">
			<label>Nhập tên ngành</label>
			<input type="text" name="ten_nganh" id="nganh" class="form-control" style="width: 30%;">
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Thêm</button>
		<button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
	</form>
</body>
</html>
@endsection