<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SinhVien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('sinh_vien',function(Blueprint $TABLE){
            $TABLE -> INCREMENTS('ma');
            $TABLE -> STRING('ho_ten',50) ->nullable();
            $TABLE -> BOOLEAN('gioi_tinh') ->nullable();
            $TABLE -> DATE('ngay_sinh') ->nullable();
            $TABLE -> TEXT('dia_chi') ->nullable();
            $TABLE -> CHAR('so_dien_thoai') -> nullable();
            $TABLE -> TEXT('email') -> unique() -> nullable();
            $TABLE -> INTEGER('ma_lop') -> unsigned() -> nullable();

            $TABLE -> foreign('ma_lop') -> references('ma') -> on('lop')-> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
