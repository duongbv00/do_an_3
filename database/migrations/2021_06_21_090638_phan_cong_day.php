<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PhanCongDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('phan_cong_day',function(Blueprint $TABLE){
            $TABLE -> INTEGER('ma_lop') -> unsigned();
            $TABLE -> INTEGER('ma_mon') -> unsigned();
            $TABLE -> INTEGER('ma_giao_vien') -> unsigned();

            $TABLE -> foreign('ma_lop') -> references('ma') -> on('lop')-> onDelete('cascade');
            $TABLE -> foreign('ma_mon') -> references('ma') -> on('mon_hoc')-> onDelete('cascade');
            $TABLE -> foreign('ma_giao_vien') -> references('ma') -> on('giao_vien')-> onDelete('cascade');
            $TABLE -> primary(['ma_lop','ma_mon']);
        });
    }

    /**ma
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
