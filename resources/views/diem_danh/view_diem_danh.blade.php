@extends('layout.master.master-gv')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{--<script>
		var count;
		document.getElementById("action").onclick = function (e) {
			if (this.checked){
				count ++;
				document.getElementById('dh').
			}
			else{
				count --;
			}
			return true;
		};
	</script>--}}
</head>
<body>
	@if (Session::has('error'))
		<h1>{{ Session::get('error') }}</h1>
	@endif
	<h3>Điểm Danh Sinh Viên</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('diem_danh.view_diem_danh') }}" class="custom-validation" style="margin-left: 40%">
		{{ csrf_field() }}
		<div class="form-group">
			<label style="margin-left: 100px;">Lớp</label>
			<select name="ma_lop" class="form-control" style="width: 30%;padding-left: 75px">
				@foreach ($array_lop as $lop)
					<option value="{{ $lop -> ma }}" @if ($lop -> ma == $ma_lop)
						selected 
					@endif> 
					{{ $lop -> ten_lop }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label style="margin-left: 80px">Môn học</label>
			<select name="ma_mon_hoc" class="form-control" style="width: 30%;padding-left: 75px">
				@foreach ($array_mon as $mon_hoc)
					<option value="{{ $mon_hoc -> ma }}" @if ($mon_hoc -> ma == $ma_mon_hoc)
						selected 
					@endif> {{ $mon_hoc -> ten_mon }} </option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1" style="margin-left: 80px">Chọn</button>
	</form>
	<br>
	<br>
	<form method="POST" action="{{ route('diem_danh.process_diem_danh') }}">
		{{ csrf_field() }}
		<input type="hidden" name="ma_lop" value="{{ $ma_lop }}">
		<input type="hidden" name="ma_mon_hoc" value="{{ $ma_mon_hoc }}">
		<table class="table mb-0" style="text-align: center; width: 100%;">
			<tr>
				<th>
					<label>Sinh viên</label>
				</th>
				<th>
					<label>Tình trạng </label>
				</th>
			</tr>
			@foreach ($array_sinh_vien as $sinh_vien )
			<tr>
				<td width="30%">
					<label>
						{{ $sinh_vien -> ho_ten }} &ensp;(0/0)
					</label>
				</td>
				<td width="70%">
					<input type="radio" id = "action" name="tinh_trang_di_hoc[{{ $sinh_vien -> ma }}]" value="1" @if (isset($array[$sinh_vien -> ma]) && $array[$sinh_vien -> ma] == 1)
						checked 
					@endif checked > &ensp; Đi Học &emsp;&emsp;&emsp;&emsp;&emsp;
					<input type="radio" name="tinh_trang_di_hoc[{{ $sinh_vien -> ma }}]" value="2" @if (isset($array[$sinh_vien -> ma]) && $array[$sinh_vien -> ma] == 2)
						checked 
					@endif> &ensp; Muộn&emsp;&emsp;&emsp;&emsp;&emsp;
					<input type="radio" name="tinh_trang_di_hoc[{{ $sinh_vien -> ma }}]" value="3" @if (isset($array[$sinh_vien -> ma]) && $array[$sinh_vien -> ma] == 3)
						checked 
					@endif> &ensp; Nghỉ Học &emsp;&emsp;&emsp;&emsp;&emsp;
					<input type="radio" name="tinh_trang_di_hoc[{{ $sinh_vien -> ma }}]" value="4" @if (isset($array[$sinh_vien -> ma]) && $array[$sinh_vien -> ma] == 4)
						checked 
					@endif> &ensp; Nghỉ Có Phép
				</td>
			</tr>
			@endforeach
		</table>
		<br>
		<br>
		<table width="100%">
			<tr>
				<td width="25%">
					<label>Đi Học: {{ $count_dh }}</label>
				</td>
				<td width="25%">
					<label>Muộn học:&ensp;0</label>
				</td>
				<td width="25%">
					<label>Nghỉ:&ensp;0</label>
				</td>
				<td width="25% ">
					<label>Nghỉ có phép:&ensp;0</label>
				</td>
			</tr>
		</table>
		<br>
		<br>
		<div class="form-group" style="padding-left: 42%;">
			<label>Thời Gian Từ</label>
			<input type="time" name="thoi_gian_bat_dau" value="08:00" class="form-control" style="width: 30%; text-align: center;">
		</div>
		<div class="form-group" style="padding-left: 42%;">
			<label>Đến</label>
			<input type="time" name="thoi_gian_ket_thuc" value="12:00" class="form-control" style="width: 30%; text-align: center;">
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1" style="margin-left: 47%;">Diem danh</button>
		<br>
	</form>
</body>
</html>
@endsection