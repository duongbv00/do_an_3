@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h3>Danh sách phân công</h3>
	<br>
	<table border="1" width="50%" class="table table-bordered mb-0" style="text-align: center;">
		<tr>
			<th>Giáo viên</th>
			<th>Lớp</th>
			<th>Môn học</th>
			<th>Tác vụ</th>
		</tr>
		@foreach ($array_phan_cong as $phan_cong)
			<tr>
				<td>
					{{ $phan_cong -> giao_vien -> ho_ten }}
				</td>
				<td>
					{{ $phan_cong -> lop -> ten_lop }}
				</td>
				<td>
					{{ $phan_cong -> mon_hoc -> ten_mon }}
				</td>
				<td>
					<table style="width: 100%;">
						<td>
							<a href="{{ route('phan_cong.view_update',['ma_lop' => $phan_cong->ma_lop,'ma_mon' => $phan_cong->ma_mon]) }}">
								<i class="mdi mdi-pencil-outline"></i>
							</a>
						</td>
						<td>
							<a href="{{ route('phan_cong.delete',['ma_lop' => $phan_cong->ma_lop,'ma_mon' => $phan_cong->ma_mon]) }}">
								<i class="dripicons-trash"></i>
							</a>
						</td>
					</table>
				</td>
			</tr>
		@endforeach
	</table>
	<span style="text-align: center">{{ $array_phan_cong -> render() }}</span>
	<br>
	<button class="btn btn-primary waves-effect waves-light mr-1">
		<a href="{{ route('phan_cong.view_insert') }}" style="color: #FFF">Phân Công Giáo Viên</a>
	</button>
</body>
</html>
@endsection