<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiaoVu extends Model
{
    protected $table = 'giao_vu';
    protected $fillable = [
        'ho_ten',
        'email',
        'password',
        'token'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
