<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
     <div id="sidebar-menu">
        <!-- Left Menu Start -->
        <ul class="metismenu list-unstyled" id="side-menu">
            <li class="menu-title">MENU</li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <span class="badge badge-pill badge-primary float-right"></span>
                    <i class="ion ion-md-school"></i>
                    <span> Sinh Viên </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('sinh_vien.view_insert_all') }}">Thêm Sinh Viên</a></li>
                    <li><a href="{{ route('sinh_vien.view_all') }}">Danh Sách Sinh Viên</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <span class="badge badge-pill badge-primary float-right"></span>
                    <i class="dripicons-user"></i>
                    <span> Giáo Viên </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('GiaoVien.view_insert') }}">Đăng kí tài khoản</a></li>
                    <li><a href="{{ route('phan_cong.view_all') }}">Phân Công</a></li>
                    <li><a href="{{ route('GiaoVien.view_all') }}">Danh Sách Giáo Viên</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <span class="badge badge-pill badge-primary float-right"></span>
                    <i class="mdi mdi-view-dashboard"></i>
                    <span> Liên Khóa </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('khoa.view_insert') }}">Thêm Khóa</a></li>
                    <li><a href="{{ route('khoa.view_all') }}">Danh Sách Liên Khóa</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <span class="badge badge-pill badge-primary float-right"></span>
                    <i class="mdi mdi-home-edit"></i>
                    <span> Lớp Học </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('lop.view_insert') }}">Thêm Lớp</a></li>
                    <li><a href="{{ route('lop.view_all') }}">Danh Sách Lớp</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <span class="badge badge-pill badge-primary float-right"></span>
                    <i class="mdi mdi-flask-outline"></i>
                    <span> Ngành Học </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('nganh_hoc.view_insert') }}">Thêm Ngành</a></li>
                    <li><a href="{{ route('nganh_hoc.view_all') }}">Danh Sách Ngành Học</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <span class="badge badge-pill badge-primary float-right"></span>
                    <i class="mdi mdi-buffer"></i>
                    <span> Môn Học </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('mon_hoc.view_insert') }}">Thêm Môn Học</a></li>
                    <li><a href="{{ route('mon_hoc.view_all') }}">Danh Sách Môn Học</a></li>
                </ul>
            </li>
            <li>
                <a href="javascript: void(0);" class="waves-effect">
                    <i class="ti-clipboard"></i>
                    <span> Điểm Danh </span>
                </a>
                <ul class="sub-menu" aria-expanded="false">
                    <li><a href="{{ route('diem_danh.view_all') }}">Điểm Danh</a></li>
                    <li><a href="{{ route('diem_danh.lich_su') }}">Lịch Sử Điểm Danh</a></li>
                </ul>
            </li>
        </ul>
    </div>
</body>
</html>