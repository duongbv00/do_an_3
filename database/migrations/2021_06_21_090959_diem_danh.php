<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DiemDanh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('diem_danh',function(Blueprint $TABLE){
            $TABLE -> INCREMENTS('ma');
            $TABLE -> INTEGER('ma_giao_vien') -> unsigned();
            $TABLE -> INTEGER('ma_lop') -> unsigned();
            $TABLE -> INTEGER('ma_mon') -> unsigned();
            $TABLE -> DATE('ngay_diem_danh');
            $TABLE -> TIME('thoi_gian_bat_dau');
            $TABLE -> TIME('thoi_gian_ket_thuc');

            $TABLE -> foreign('ma_giao_vien') -> references('ma') -> on('giao_vien')-> onDelete('cascade');
            $TABLE -> foreign(['ma_lop','ma_mon']) -> references(['ma_lop','ma_mon']) -> on('phan_cong_day')-> onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
