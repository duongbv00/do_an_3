@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('lop').value;
			var khoa = document.getElementById('ma_khoa').value;
			var nganh = document.getElementById('ma_nganh').value;
			if (name == "") {
				alert('Tên không được bỏ trống');
				return false;
			}
			if (khoa == "0"){
				alert('Vui lòng chọn liên khóa.');
				return false;
			}
			if (nganh == "0"){
				alert('Vui lòng chọn ngành.');
				return false;
			}
			return true;
		}
	</script>
</head>
<body>
	<div>
		@if (Session::has('error'))
			<script>
				alert("Lớp lày đã tồn tại.");
			</script>
		@endif
	</div>
	<h3>Thêm lớp</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('lop.process_insert') }}" onsubmit="return validateForm()">
		{{ csrf_field() }}
		<div class="form-group">
			<label>Nhập tên lớp</label>
			<input type="text" name="ten_lop" id="lop" class="form-control" style="width: 30%;">
		</div>
		<div class="form-group">
			<label>Liên khóa</label>
			<select name="ma_khoa" id="ma_khoa" class="form-control" style="width: 10%;">
				<option selected disabled value="0">----Chọn----</option>
				@foreach ($array_khoa as $khoa)
					<option value="{{ $khoa -> ma }}">
						{{ $khoa -> ten_khoa }}
					</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label>Ngành học</label>
			<select name="ma_nganh" id="ma_nganh" class="form-control" style="width: 10%;">
				<option selected disabled value="0">----Chọn----</option>
				@foreach ($array_nganh as $nganh)
					<option value="{{ $nganh -> ma }}">
						{{ $nganh -> ten_nganh }}
					</option>
				@endforeach
			</select>
		</div>
		<br>
		<button class="btn btn-primary waves-effect waves-light mr-1">Thêm</button>
		<button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
	</form>
</body>
</html>
@endsection