@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('khoa').value;
			if (name == "") {
				alert("Tên không được để trống:");
				return false;
			}
			return true;
		}
	</script>
</head>
<body>
	<div>
		@if (Session::has('erro'))
			<script>
				alert("Tên lày đã tồn tại.");
			</script>
		@endif
	</div>
	<h3>Thêm Khóa</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('khoa.process_insert') }}" onsubmit="return validateForm()">
		{{ csrf_field() }}
		<div class="form-group">
			<label>Nhập tên khóa</label>
			<input type="text" name="ten_khoa" id="khoa" class="form-control" style="width: 30%">
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Thêm</button>
		<button type="reset" class="btn btn-secondary waves-effect">Xóa</button>
	</form>
</body>
</html>
@endsection