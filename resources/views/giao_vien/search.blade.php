@extends('layout.master.master')
@section('search')
    <div class="search-bar">
        <form action="{{ route('GiaoVien.search') }}" method="POST" class="search-bar ">
            @csrf
            <input class="search-input form-control" type="search" name="search" placeholder="Tìm kiếm giáo viên" />
            <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                <i class="mdi mdi-close-circle"></i>
            </a>
        </form>
    </div>
@endsection
@section('content')

    <div>
        @if (Session::has('session_insert'))
            <script>
                alert("Đăng kí thành công.");
            </script>
        @endif

        @if (Session::has('session_update'))
            <script>
                alert("Sửa đổi thành công.");
            </script>
        @endif
    </div>
    <div class="row">
        <h3>Danh sách giáo viên</h3>
        <br>
        <br>
        <br>
        <br>
        <table border="1" width="50%" class="table table-bordered mb-0" style="text-align: center;">
            <tr>
                <th>Họ tên</th>
                <th>Giới tính</th>
                <th>Ngày sinh</th>
                <th>Địa chỉ</th>
                <th>Email</th>
                <th>Tác vụ</th>
            </tr>
            @foreach ($array_giao_vien as $giao_vien)
                <tr>
                    <td>
                        {{ $giao_vien -> ho_ten }}
                    </td>
                    <td>
                        {{ $giao_vien -> gioi_tinh }}
                    </td>
                    <td>
                        {{ $giao_vien -> ngay_sinh }}
                    </td>
                    <td>
                        {{ $giao_vien -> que_quan}}
                    </td>
                    <td>
                        {{ $giao_vien -> email }}
                    </td>
                    <td>
                        <table style="width: 100%; border: none;">
                            <td>
                                <a href="{{ route('GiaoVien.view_update',['ma' => $giao_vien->ma]) }}">
                                    <i class="mdi mdi-pencil-outline"></i>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('GiaoVien.delete',['ma' => $giao_vien->ma]) }}">
                                    <i class="dripicons-trash"></i>
                                </a>
                            </td>
                        </table>
                    </td>
                </tr>
            @endforeach
        </table>
        <br>
        <span style="text-align: center">{{ $array_giao_vien -> render() }}</span>
    </div>
@endsection
