@extends('layout.master.master')
@section('search')
    <div class="search-bar">
        <form action="{{ route('khoa.search') }}" method="POST" class="search-bar ">
            @csrf
            <input class="search-input form-control" type="search" name="search" placeholder="Tìm kiếm liên khóa" />
            <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                <i class="mdi mdi-close-circle"></i>
            </a>
        </form>
    </div>
@endsection
@section('content')

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>

    </head>
    <body>
    <div>
        @if (Session::has('session'))
            <script>
                alert("Thêm thành công.");
            </script>
        @elseif (Session::has('session_update'))
            <script>
                alert("Sửa thành công.");
            </script>
        @endif
    </div>
    <h3>Danh Sách Khóa</h3>
    <br>
    <br>
    <table class="table table-bordered mb-0" style="width: 70%; text-align: center">
        <tr>
            <th>Mã khóa</th>
            <th>Tên khóa</th>
            <th>Tác vụ</th>
        </tr>
        @foreach ($array_khoa as $khoa)
            <tr>
                <td>
                    {{ $khoa -> ma }}
                </td>
                <td>
                    {{ $khoa -> ten_khoa }}
                </td>
                <td>
                    <table style="width: 100%">
                        <td>
                            <a href="{{ route('khoa.view_update',['ma' => $khoa->ma]) }}">
                                <i class="mdi mdi-pencil-outline"></i>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('khoa.delete',['ma' => $khoa->ma]) }}">
                                <i class="dripicons-trash"></i>
                            </a>
                        </td>
                    </table>
                </td>
            </tr>
        @endforeach
    </table>
    <span style="text-align: center">{{ $array_khoa -> render() }}</span>
    </body>
    </html>
@endsection