<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NganhHoc extends Model
{
    protected $table = 'nganh_hoc';
    protected $fillable = [
        'ten_nganh'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
