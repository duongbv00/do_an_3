@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html lang="">
<head>
	<title></title>
	<style>
		#show{
			position: relative;
			bottom: 30px;
			left: 500px;
		}
		span {
			color: #707070;
			cursor: pointer;
		}
	</style>
	<script>
		function validateForm() {
			var name = document.getElementById('ho_ten').value;
			var email = document.getElementById('email').value;
			var password = document.getElementById('password').value;
			if (name == "") {
				alert("Tên không được để trống:");
				return false;
			}
			if (email == "") {
				alert("Email không được để trống:");
				return false;
			}
			if (password == "") {
				alert("Password không được để trống.");
				return false;
			}else{
				var result = confirm("Bạn có muốn chỉnh sửa không ?");
				if(result == true){
					return true;
				}else {
					return false;
				}
			}

		}
		isBoon = true;
		function ShowHidden(){
			if(isBoon){
				document.getElementById("password").setAttribute("type","text");
				isBoon = false;
			}else {
				document.getElementById("password").setAttribute("type","password");
				isBoon = true;
			}
		}
	</script>
</head>
<body>
	<h3>Cập Nhật Thông Tin Giáo Viên</h3>
	<br>
	<br>
	@if (Session::has('erro'))
		<script>
			alert("Email đã tồn tại.");
		</script>
	@endif
	<form method="POST" onsubmit="return validateForm()" class="custom-validation" action="{{ route('GiaoVien.update',['ma' => $giao_vien -> ma]) }}">
		@csrf
		<div class="form-group">
			<label>Họ và tên</label>
			<input type="text" name="ho_ten" id = "ho_ten" value="{{ $giao_vien -> ho_ten }}"class="form-control" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>Giới tính</label>&emsp;
			<input type="radio" name="gioi_tinh" value ="0" 
			@if ($giao_vien -> gioi_tinh == 0)
				checked 
			@endif>Nam &ensp;
			<input type="radio" name="gioi_tinh" value ="1" 
			@if ($giao_vien -> gioi_tinh ==1)
				checked 
			@endif
			>Nữ
		</div>
		<div class="form-group">
			<label>Ngày sinh</label>
			<input type="date" name="ngay_sinh" id="name" value="{{ $giao_vien -> ngay_sinh }}"class="form-control" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>Đỉa chỉ</label>
			<input type="text" name="que_quan" value="{{ $giao_vien -> que_quan }}"class="form-control" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="email" name="email" id="email" value="{{ $giao_vien -> email }}"class="form-control" style="width: 40%;">
		</div>
		<div class="form-group">
			<label>Password</label>
			<input type="password" name="password" id="password" value="{{ $giao_vien -> password }}"class="form-control" style="width: 40%;">
			<span id = "show">
				<span class="far fa-eye" onclick="ShowHidden()"></span>
			</span>
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Lưu</button>
		<button type="reset" class="btn btn-secondary waves-effect">Khôi phục</button>
	</form>
</body>
</html>
@endsection