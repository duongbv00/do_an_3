<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SinhVien;
use App\Models\Lop;
use App\Models\NganhHoc;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SinhVienImport;

class SinhVienController extends Controller
{
    public function view_all()
    {
        $array_sinh_vien = SinhVien::join('lop','sinh_vien.ma_lop','=','lop.ma')
            -> select('sinh_vien.*','lop.ten_lop') -> paginate(10);
        return view('sinh_vien.view_all',compact('array_sinh_vien'));
    }

    public function view_insert_all()
    {
        return view('sinh_vien.view_insert_all');
    }
    public function view_insert()
    {
        $lop = Lop::get();
        return view('sinh_vien.view_insert',compact('lop'));
    }

    public function insert_excel()
    {
        return view('sinh_vien.insert_excel');
    }
    public function process_excel(Request $rq)
    {
        Excel::import(new SinhVienImport, $rq -> file_excel);
        return redirect('sinh_vien');
    }
    public function process_insert(Request $rq)
    {
        SinhVien::create($rq -> all());
        return redirect('sinh_vien') -> with('success','/');
    }
    public function view_update($ma)
    {
        $lop = Lop::get();
        $sinh_vien = SinhVien::where('ma', $ma) -> first();
        return view('sinh_vien.view_update',compact('sinh_vien','lop'));
    }
    public function update($ma,Request $rq)
    {
        SinhVien::find($ma)-> update($rq -> all());
        return redirect() -> route('sinh_vien.view_all');
    }
    public function search(Request $rq){
        $data = $rq -> search;
        $array_sinh_vien = SinhVien::where('ho_ten','LIKE', '%'.$data.'%')
                            -> orWhere('dia_chi','LIKE', '%'.$data.'%')
                            -> orWhere('so_dien_thoai','LIKE', '%'.$data.'%')
                            -> orWhere('ngay_sinh','LIKE', '%'.$data.'%')
                            -> orWhere('email','LIKE', '%'.$data.'%')
                            -> join('lop','sinh_vien.ma_lop','=','lop.ma')
                            -> orWhere('lop.ten_lop','LIKE', '%'.$data.'%')
                            -> paginate(10);
        return view('sinh_vien.search',compact('array_sinh_vien'));
    }
    public function delete(Request $rq)
    {
        $ma = $rq -> delete;
        foreach ($ma as $ma){
            SinhVien::where('ma',$ma) -> delete();
        }
        return redirect('sinh_vien');

    }
}
