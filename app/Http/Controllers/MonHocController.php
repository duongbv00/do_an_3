<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MonHoc;

class MonHocController extends Controller
{
    public function view_all()
    {
        $array_mon_hoc = MonHoc::paginate(10);
        return view('mon_hoc.view_all',compact('array_mon_hoc'));
    }
    public function view_insert()
    {
        return view('mon_hoc.view_insert');
    }
    public function process_insert(Request $rq)
    {
        $mon = $rq -> ten_mon;
        if(MonHoc::where('ten_mon','=',$mon) -> first()){
            return redirect() -> route('mon_hoc.view_insert') -> with('error','/');
        }
        MonHoc::create($rq -> all());
        return redirect('mon_hoc') -> with('success','/');
    }
    public function view_update($ma,Request $rq)
    {
        $mon_hoc = MonHoc::find($ma);
        return view('mon_hoc.view_update',compact('mon_hoc'));
    }
    public function update($ma,Request $rq)
    {
        $mon = $rq -> ten_mon;
        if(MonHoc::where('ma','=',$ma) -> where('ten_mon','=',$mon) ->first()){
            NganhHoc::find($ma) -> update($rq -> all());
        }
        else if(MonHoc::where('ten_mon','=',$mon) -> first()){
            return redirect() -> route('mon_hoc.view_update',['ma' => $ma]) -> with('error','/');
        }
        MonHoc::find($ma) -> update($rq -> all());
        return redirect('mon_hoc') -> with('success_update','/');
    }
    public function delete($ma)
    {
        MonHoc::destroy($ma);
        return redirect('mon_hoc');
    }
    public function search(Request $rq){
        $data = $rq -> search;
        $array_mon_hoc = MonHoc::where('ten_mon','LIKE', '%'.$data.'%') -> paginate(10);

        return view('mon_hoc.search',compact('array_mon_hoc'));
    }
}
