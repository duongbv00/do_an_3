<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiaoVien extends Model
{
    protected $table = 'giao_vien';
    protected $fillable = [
        'ho_ten',
        'gioi_tinh',
        'ngay_sinh',
        'dia_chi',
        'email',
        'password',
        'token'
    ];
    public $timestamps = false;
    protected $primaryKey = 'ma';
}
