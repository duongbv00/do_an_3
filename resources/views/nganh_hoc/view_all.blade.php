@extends('layout.master.master')
@section('search')
	<div class="search-bar">
		<form action="{{ route('nganh_hoc.search') }}" method="POST" class="search-bar ">
			@csrf
			<input class="search-input form-control" type="search" name="search" placeholder="Tìm kiếm ngành học" />
			<a href="#" class="close-search toggle-search" data-target="#search-wrap">
				<i class="mdi mdi-close-circle"></i>
			</a>
		</form>
	</div>
@endsection
@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script>
		function allow(){
			var check = confirm("Bạn muốn xóa không");
			if (check == true){
				return true;
			}else {
				return false;
			}
		}
	</script>
</head>
<body>
<div>
	@if (Session::has('success'))
		<script>
			alert("Thêm thành công.");
		</script>
	@elseif(Session::has('success_update'))
		<script>
			alert("sửa thành công.");
		</script>
	@endif
</div>
	<h1>Danh Sách Ngành Học</h1>
	<br>
	<br>
	<table class="table table-bordered mb-0" style="text-align: center; width: 70%">
		<tr>
			<th>Mã ngành</th>
			<th>Tên ngành</th>
			<th>Tác vụ</th>
		</tr>
		@foreach ($array_nganh as $nganh)
			<tr>
				<td>
					{{ $nganh -> ma }}
				</td>
				<td>
					{{ $nganh -> ten_nganh }}
				</td>
				<td>
					<table style="width: 100%">
						<td>
							<a href="{{ route('nganh_hoc.view_update',['ma' => $nganh->ma]) }}">
								<i class="mdi mdi-pencil-outline"></i>
							</a>
						</td>
						<td>
							<a href="{{ route('nganh_hoc.delete',['ma' => $nganh->ma]) }}" onclick="return allow()">
								<i class="dripicons-trash"></i>
							</a>
						</td>
					</table>
				</td>
			</tr>
		@endforeach
	</table>
<span style="text-align: center">{{ $array_nganh -> render() }}</span>
</body>
</html>
@endsection