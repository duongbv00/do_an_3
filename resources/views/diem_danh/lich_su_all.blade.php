@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table class="table table-bordered mb-0" style="text-align: center;">
		<tr>
			<th>Giáo Viên</th>
			<th>Tên lớp</th>
			<th>Tên Môn</th>
			<th>Thời gian</th>
			<th>Ngày Điểm danh</th>
			<th>Hành Động</th>
		</tr>
		@foreach ($array_diem_danh as $diem_danh)
		<tr>
			<td> 
				{{ $diem_danh -> ho_ten }}</td>
			<td>
				{{ $diem_danh -> ten_lop }}
			</td>
			<td>
				{{ $diem_danh -> ten_mon_hoc }}
			</td>
			<td>
				{{ $diem_danh -> thoi_gian_bat_dau }} - {{ $diem_danh -> thoi_gian_ket_thuc }}
			</td>
			<td>
				{{ $diem_danh -> ngay_diem_danh }}
			</td>
			<td>
				<a href="{{ route('diem_danh.view_update',['ma' => $diem_danh -> ma]) }}">Xem</a>
			</td>
		</tr>
		@endforeach
	</table>
</body>
</html>
@endsection
