@extends('layout.master.master-gv')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	@if (Session::has('error'))
		<h1>{{ Session::get('error') }}</h1>
	@endif
	<h3>Điểm Danh Sinh Viên</h3>
	<br>
	<br>
	<form method="POST" {{ route('diem_danh.view_diem_danh') }} class="custom-validation" style="padding-left: 40%;">
		{{ csrf_field() }}
		<div class="form-group">	
			<label style="padding-left: 100px;">Lớp</label>
			<select name="ma_lop" class="form-control" style="width: 30%;padding-left: 75px">
				<option selected disabled >---Chọn---</option>
				@foreach ($array_lop as $lop)
					<option value="{{ $lop -> ma }}"> {{ $lop -> ten_lop }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label style="padding-left: 80px;">Môn Học</label>
			<select name="ma_mon_hoc" class="form-control" style="width: 30%;padding-left: 75px;">
				<option selected disabled>---Chọn---</option>
				@foreach ($array_mon as $mon_hoc)
					<option value="{{ $mon_hoc -> ma }}"> {{ $mon_hoc -> ten_mon }} </option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1"  style="margin-left: 80px;">Chọn</button>
	</form>
</body>
</html>
@endsection