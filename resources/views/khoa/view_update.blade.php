@extends('layout.master.master')

@section('content')

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript">
		function validateForm() {
			var name = document.getElementById('khoa').value;
			if (name == "") {
				alert("Tên không được để trống:");
				return false;
			}else{
				var result = confirm("Bạn có muốn chỉnh sửa không ?");
				if(result == true){
					return true;
				}else {
					return false;
				}
			}
			return true;
		}
	</script>
</head>
<body>
	<div>
		@if (Session::has('error'))
			<script>
				alert("Tên lày đã tồn tại.");
			</script>
		@endif
	</div>
	<h3>Sửa Thông Tin</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('khoa.update',['ma' => $khoa -> ma]) }}" onsubmit="return validateForm()">
		{{ csrf_field() }}
		<div class="form-group">
			<label>Nhập tên ngành</label>
			<input type="text" name="ten_khoa" id="khoa" value="{{ $khoa -> ten_khoa }}" class="form-control" style="width: 30%">
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Sửa</button>
		<button type="reset" class="btn btn-secondary waves-effect">Khôi phục</button>
	</form>
</body>
</html>
@endsection