<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GiaoVien;
use App\Models\GiaoVu;

class GiaoVienController extends Controller
{
    public function view_all()
    {
        $array_giao_vien = GiaoVien::paginate(10);
        return view('giao_vien.view_all',compact('array_giao_vien'));
    }
    public function view_insert()
    {
        return view('giao_vien.view_insert');
    }
    public function process_insert(Request $rq)
    {
        $giao_vien = $rq -> email;
        if(GiaoVu::where('email','=',$giao_vien) -> first()){
            return redirect() -> route('GiaoVien.view_insert') -> with('erro','Email Đã Tồn Tại');
        }
        else if(GiaoVien::where('email','=',$giao_vien) -> first()){
            return redirect() -> route('GiaoVien.view_insert') -> with('erro','Email Đã Tồn Tại');
        }
        GiaoVien::create($rq -> all());
        return redirect('giao_vien') -> with('session_insert','#');
    }
    public function view_update($ma,Request $rq)
    {
        $giao_vien = GiaoVien::find($ma);
        return view('giao_vien.view_update',compact('giao_vien'));
    }
    public function update($ma,Request $rq)
    {
        $giao_vien = $rq -> email;
        if(GiaoVien::where('ma','=',$ma) -> where('email','=',$giao_vien) ->first()){
            GiaoVien::find($ma) -> update($rq -> all());
        }
        else if(GiaoVu::where('email','=',$giao_vien) -> first()){
            return redirect() -> route('GiaoVien.view_update',['ma' => $ma]) -> with('erro','Email Đã Tồn Tại');
        }
        else if(GiaoVien::where('email','=',$giao_vien) -> first()){
            return redirect() -> route('GiaoVien.view_update',['ma' => $ma] ) -> with('erro','Email Đã Tồn Tại');
        }
        GiaoVien::find($ma) -> update($rq -> all());
        return redirect() -> route('GiaoVien.view_all') -> with('session_update','#');
    }
    public function delete($ma)
    {
        GiaoVien::destroy($ma);
        return redirect() -> route('GiaoVien.view_all');
    }
    public function search(Request $rq){
        $data = $rq -> search;
        $array_giao_vien = GiaoVien::where('ho_ten','LIKE', '%'.$data.'%')
                            -> orWhere('dia_chi','LIKE', '%'.$data.'%')
                            -> orWhere('ngay_sinh','LIKE', '%'.$data.'%')
                            -> orWhere('email','LIKE', '%'.$data.'%')
                            -> paginate(10);

        return view('giao_vien.search',compact('array_giao_vien'));
    }
}
