<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhanCongDay extends Model
{
    use Traits\HasCompositePrimaryKey;
    protected $table = 'phan_cong_day';
    protected $fillable = [
        'ma_lop',
        'ma_mon',
        'ma_giao_vien',
    ];
    public $timestamps = false;
    protected $primaryKey = ['ma_lop','ma_mon'];

    public function lop()
    {
        return $this -> belongsTo('App\Models\Lop','ma_lop');
    }
    public function mon_hoc()
    {
        return $this -> belongsTo('App\Models\MonHoc','ma_mon');
    }
    public function giao_vien()
    {
        return $this -> belongsTo('App\Models\GiaoVien','ma_giao_vien');
    }
}
