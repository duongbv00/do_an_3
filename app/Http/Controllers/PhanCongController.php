<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\NganhHoc;
use App\Models\MonHoc;
use App\Models\GiaoVien;
use App\Models\PhanCongDay;
use Session;

class PhanCongController extends Controller
{
    public function view_all() {
        $array_phan_cong = PhanCongDay::with('lop','mon_hoc','giao_vien') -> paginate(10);
        return view('phan_cong.view_all',compact('array_phan_cong'));
    }
    public function gv_phan_cong($value=''){
        $array_phan_cong = PhanCongDay::with('lop','mon_hoc','giao_vien')
            -> where('ma_giao_vien',Session::get('ma_giao_vien'))
            -> get();

        return view('phan_cong.gv_phan_cong',compact('array_phan_cong'));
    }
    public function view_insert() {
        $lop = Lop::get();
        $nganh = NganhHoc::get();
        $giao_vien = GiaoVien::get();
        $mon_hoc = MonHoc::get();
        return view('phan_cong.view_insert',compact('lop','giao_vien','mon_hoc','nganh'));
    }

    public function ajax_nganh( Request $rq) {
        $array_lop = Lop::where('ma_nganh',$rq -> ma_nganh) -> get();
        return $array_lop;
    }

    public function process_insert(Request $rq){
        foreach ($rq -> mon_hoc as $ma_mon_hoc) {
            PhanCongDay::updateOrCreate([
                'ma_lop' => $rq -> ma_lop,
                'ma_mon' => $ma_mon_hoc,
            ],[
                'ma_giao_vien' => $rq -> ma_giao_vien
            ]);
        }
        return redirect('phan_cong');
    }

    public function view_update($ma_lop,$ma_mon,Request $rq){
        $nganh_hoc = $rq -> nganh_hoc;
        $lop = Lop::get();
        $nganh = NganhHoc::get();
        $giao_vien = GiaoVien::get();
        $mon_hoc = MonHoc::get();
        $array_phan_cong = PhanCongDay::where('ma_lop',$ma_lop)
            -> where('ma_mon',$ma_mon)
            -> join('lop','lop.ma','phan_cong_day.ma_lop')
            -> join('giao_vien','giao_vien.ma','phan_cong_day.ma_giao_vien')
            -> join('mon_hoc','mon_hoc.ma','phan_cong_day.ma_mon')
            -> first();
        return view('phan_cong.view_update',compact('array_phan_cong','lop','giao_vien','mon_hoc','nganh'));
    }
    public function delete($ma_lop,$ma_mon){
        $array_mon = PhanCongDay::where('ma_lop',$ma_lop)
            -> where('ma_mon',$ma_mon) -> first() -> delete();
        return redirect('phan_cong');
    }
}
