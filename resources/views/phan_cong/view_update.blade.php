@extends('layout.master.master')

@section('content')
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#nganh").change(function() {
				var nganh = $(this).val();
				$.ajax({
					url: '{{ route('phan_cong.ajax_nganh') }}',
					type: 'GET',
					dataType: 'json',
					data: {ma_nganh:nganh},
				})
				.done(function(response) {
					$("#lop").html('');
					$("#lop").append('<option selected disabled >---Chọn---</option>');
					$(response).each(function() {
						$("#lop").append(`
							<option value='${this.ma}'> ${this.ten_lop} </option>
						`);
					});
					console.log("success");
				})
				.fail(function() {
					console.log("error");
				})
				
			});
		});
	</script>
</head>
<body>
	<h3>Sửa Phân Công</h3>
	<br>
	<br>
	<form method="POST" action="{{ route('phan_cong.process_insert') }}">
		{{ csrf_field() }}
		<div class="form-group">
		Giáo Viên
			<select name="ma_giao_vien" class="form-control" style="width: 40%">
				@foreach ($giao_vien as $giao_vien)
					<option value="{{ $giao_vien -> ma }}" @if ($giao_vien -> ma == $array_phan_cong -> ma_giao_vien)
						selected 
					@endif>{{ $giao_vien -> ho_ten }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
		Ngành Học
			<select name="ma_nganh" id="nganh" class="form-control" style="width: 40%">
				@foreach ($nganh as $nganh)
					<option value="{{ $nganh -> ma }}" name ="nganh_hoc"  
						@if ($nganh -> ma == $array_phan_cong -> ma_nganh)
						selected 
					@endif >{{ $nganh -> ten_nganh }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			Lớp
			<select name="ma_lop" id="lop" class="form-control" style="width: 40%">
				@foreach ($lop as $lop)
					<option value="{{ $lop -> ma }}"
						@if ($lop -> ma == $array_phan_cong -> ma_lop)
						selected 
						@endif
					> {{ $lop -> ten_lop }} </option>
				@endforeach
			</select>
			<br>
		</div>
		<div class="form-group">
			Tên môn học:<br>
			@foreach ($mon_hoc as $mon_hoc)
				<input type="checkbox" name="mon_hoc[{{ $mon_hoc -> ma }}]" value="{{ $mon_hoc -> ma }}"
					   @if ($mon_hoc -> ma == $array_phan_cong -> ma_mon)
					       checked
					   @endif
				>{{ $mon_hoc -> ten_mon }}<br>
			@endforeach
		</div>
		<button class="btn btn-primary waves-effect waves-light mr-1">Phân công</button>
		<button type="reset" class="btn btn-secondary waves-effect">Khôi phục</button>
	</form>
</body>
</html>
@endsection