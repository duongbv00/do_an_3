<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DiemDanhChiTiet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema:: create('diem_danh_chi_tiet',function(Blueprint $TABLE){
            $TABLE -> INTEGER('ma_diem_danh') -> unsigned();
            $TABLE -> INTEGER('ma_sinh_vien') -> unsigned();
            $TABLE -> INTEGER('tinh_trang_di_hoc');

            $TABLE -> foreign('ma_diem_danh') -> references('ma') -> on('diem_danh')-> onDelete('cascade');
            $TABLE -> foreign('ma_sinh_vien') -> references('ma') -> on('sinh_vien')-> onDelete('cascade');
            $TABLE -> primary(['ma_diem_danh','ma_sinh_vien']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
