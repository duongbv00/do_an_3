<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GiaoVu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giao_vu',function(Blueprint $TABLE){
            $TABLE -> INCREMENTS('ma');
            $TABLE -> STRING('ho_ten',50);
            $TABLE -> TEXT('email') -> unique();
            $TABLE -> TEXT('password');
            $TABLE -> STRING('token') -> nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
